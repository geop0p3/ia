#include "Panama.h"
#include <iostream>

using std::cout;
using std::cin;

void Panama::start() {
  do {
    cout << "Can I Begin? Press 1 \n";
    cin >> input;
  } while (input != 1);
  
  cout << "Start. Boat has arrive \n";
  state = &Panama::loweringWater;
  refreshState();
}

void Panama::waiting() {
  cout << "Waiting\n";
  gatesCrossed++;
  cout << "\n\tGates crossed: " << gatesCrossed << " of " << gateNumber << "\n\n";
  
  if (gatesCrossed == gateNumber / 2) {
    canDescend = true;
  }
  
  if (!canDescend) {
    state = &Panama::loweringWater;
  } else {
    state = &Panama::risingWater;
  }
  
  if (gatesCrossed >= gateNumber) {
    state = &Panama::end;
  }
  
  refreshState();
}

void Panama::loweringWater() {
  cout << "Lowering Water... \n";
  
  do {
    cout << "May I continue? Press 1.\n";
    cin >> input;
  } while (input != 1);
  
  if (!canDescend) {
    state = &Panama::loweringBarrier;
  } else {
    state = &Panama::waiting;
  }
  
  refreshState();
}

void Panama::risingWater() {
  cout << " Rising Water\n";
  
  do {
    cout << "May I continue? Press 1\n";
    cin >> input;
  } while (input != 1);
  
  if (!canDescend) {
    state = &Panama::waiting;
  } else {
    state = &Panama::loweringBarrier;
  }
  
  refreshState();
}

void Panama::loweringBarrier() {
  cout << "Lowering Barrier\n";
  
  do {
    cout << "May I Continue\n";
    cin >> input;
  } while (input != 1);
  
  state = &Panama::moving;
  refreshState();
}

void Panama::risingBarrier() {
  cout << "Rising barrier\n";
  
  do {
    cout << "May I continue_ Press1\n";
    cin >> input;
  }while (input != 1);
  
  if (!canDescend){
    state = &Panama::risingWater;
  } else {
    state = &Panama::loweringWater;
  }
  
  refreshState();
}

void Panama::moving() {
  cout << "Moving boat\n";
  do {
    cout << "May I continue? Press 1 \n";
    cin >> input;
  } while (input != 1);
  
  state = &Panama::risingBarrier;
  refreshState();
}

void Panama::end() {
  cout << "I am on the other side. Yay. The end\n";
  state = nullptr;
  return;
}

void Panama::refreshState() {
  (this->*state)();
}
