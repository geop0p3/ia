#ifndef PANAMA_H
#define PANAMA_H

class Panama {
  bool canDescend = false;
  
  bool isStateOver = false;
  
  int input = 0;
  
  int gateNumber = 0;
  
  int gatesCrossed = 0;
  
  bool hasBoatFinishedRising = false;
  
  void refreshState();

public:

  Panama (int nGates) {
    gateNumber = nGates * 2;
    state = &Panama::start;
    refreshState();
  }
  
  void  (Panama::*state)();
  
  void start();
  void waiting();
  void loweringWater();
  void risingWater();
  void loweringBarrier();
  void risingBarrier();
  void moving();
  void end();
};

#endif
