extends KinematicBody2D

enum {
	IDLE,
	NEW_DIRECTION,
	MOVE,
	HAMBRE,
	CANSADO,
	SOCIAL,
	POPO
}

const velocidad = 25
var state = MOVE
var direction = Vector2.LEFT
var mood = "Nada"
var cansado = 0;
var hambriento = 0;
var social = 0;
var popo = 0;
var actuando = false;

func _ready():
	$mood.play("Nada")
	randomize()

func _physics_process(delta):
	if !actuando :
		match state:
			IDLE:
				if direction == Vector2.RIGHT:
					$Sprite.play("IdleR")
				if direction == Vector2.LEFT:
					$Sprite.play("IdleL")
				if direction == Vector2.UP:
					$Sprite.play("IdleU")
				if direction == Vector2.DOWN:
					$Sprite.play("IdleD")
			NEW_DIRECTION:
				if direction == Vector2.RIGHT:
					$Sprite.play("IdleR")
				if direction == Vector2.LEFT:
					$Sprite.play("IdleL")
				if direction == Vector2.UP:
					$Sprite.play("IdleU")
				if direction == Vector2.DOWN:
					$Sprite.play("IdleD")
				direction = choose([Vector2.RIGHT, Vector2.LEFT, Vector2.UP, Vector2.DOWN])
				state = MOVE
			MOVE:
				if direction == Vector2.RIGHT:
					$Sprite.play("WalkingR")
				if direction == Vector2.LEFT:
					$Sprite.play("WalkingL")
				if direction == Vector2.UP:
					$Sprite.play("WalkingU")
				if direction == Vector2.DOWN:
					$Sprite.play("WalkingD")
				move(delta)
			POPO:
				if direction == Vector2.RIGHT:
					$Sprite.play("IdleR")
				if direction == Vector2.LEFT:
					$Sprite.play("IdleL")
				if direction == Vector2.UP:
					$Sprite.play("IdleU")
				if direction == Vector2.DOWN:
					$Sprite.play("IdleD")
				if (popo > 0.75):
					$mood.play("Emergencia")
					state = NEW_DIRECTION
				if (popo < 0.75 and popo > 0.25):
					$mood.play("BienIr")
					state = NEW_DIRECTION
				if (popo < 0.25) :
					$mood.play("Tranquilo")
					state = choose([IDLE, MOVE, NEW_DIRECTION, SOCIAL, HAMBRE, CANSADO])
			SOCIAL:
				if direction == Vector2.RIGHT:
					$Sprite.play("IdleR")
				if direction == Vector2.LEFT:
					$Sprite.play("IdleL")
				if direction == Vector2.UP:
					$Sprite.play("IdleU")
				if direction == Vector2.DOWN:
					$Sprite.play("IdleD")
				if (social >= 1):
					$mood.play("Deprimido")
					state = NEW_DIRECTION
				if (social > 0.75 and social < 1):
					$mood.play("Solo")
					state = NEW_DIRECTION
				if (social < 0.25) :
					$mood.play("Satisfecho")
					state = choose([IDLE, MOVE, NEW_DIRECTION, POPO, HAMBRE, CANSADO])
			HAMBRE:
				if direction == Vector2.RIGHT:
					$Sprite.play("IdleR")
				if direction == Vector2.LEFT:
					$Sprite.play("IdleL")
				if direction == Vector2.UP:
					$Sprite.play("IdleU")
				if direction == Vector2.DOWN:
					$Sprite.play("IdleD")
				if (hambriento >= 1):
					$mood.play("Hambriento")
					state = NEW_DIRECTION
				if (hambriento > 0.5 and hambriento < 0.75):
					$mood.play("Hambre")
					state = NEW_DIRECTION
				if (hambriento < 0.25) :
					$mood.play("Lleno")
					state = choose([IDLE, MOVE, NEW_DIRECTION, POPO, SOCIAL, CANSADO])
			CANSADO:
				if direction == Vector2.RIGHT:
					$Sprite.play("IdleR")
				if direction == Vector2.LEFT:
					$Sprite.play("IdleL")
				if direction == Vector2.UP:
					$Sprite.play("IdleU")
				if direction == Vector2.DOWN:
					$Sprite.play("IdleD")
				if (cansado >= 1):
					$mood.play("DebeDormir")
					state = NEW_DIRECTION
				if (cansado > 0.5 and cansado < 0.75):
					$mood.play("Cansado")
					state = NEW_DIRECTION
				if (cansado < 0.25) :
					$mood.play("MuchaEnergia")
					state = choose([IDLE, MOVE, NEW_DIRECTION, POPO, SOCIAL, HAMBRE])

func move (delta):
	move_and_slide(direction * velocidad)

func choose(array):
	array.shuffle()
	return array.front()

func _on_Timer_timeout():
	$Timer.wait_time = choose ([1, 1.5, 2, 2.5, 3])
	state = choose ([IDLE, NEW_DIRECTION, MOVE, POPO, SOCIAL, HAMBRE, CANSADO])
	if popo < 1 :
		popo += randf()*0+0.03
	
	if hambriento < 1 :
		hambriento += randf()*0+0.05
	
	if social < 1 :
		social += randf()*0+0.03
		
	if cansado < 1 :
		cansado += 0.01
	
	$PopoMeter.value = popo
	$PopoMeter/Label.text = str(popo)
	$HambrientoMeter.value = hambriento
	$HambrientoMeter/Label.text = str(hambriento)
	$SleepMeter2.value = cansado
	$SleepMeter2/Label.text = str(cansado)
	$SocialMeter.value = social
	$SocialMeter/Label.text = str(social)


func _on_Pollo_body_entered(body):
	if (body.name ==  self.name) :
		print("Entro")
		actuando = true;
		$Accion.start(1)
		$mood.play("Loading")
		if direction == Vector2.RIGHT:
			direction = Vector2.LEFT
			$Sprite.play("IdleR")
		if direction == Vector2.LEFT:
			direction = Vector2.RIGHT
			$Sprite.play("IdleL")
		if direction == Vector2.UP:
			direction = Vector2.DOWN
			$Sprite.play("IdleU")
		if direction == Vector2.DOWN:
			direction = Vector2.UP
			$Sprite.play("IdleD")
		
		hambriento = 0
	pass # Replace with function body.


func _on_Accion_timeout():
	actuando = false
	pass # Replace with function body.


func _on_Libro_body_entered(body):
	if (body.name ==  self.name) :
		print("Entro")
		actuando = true;
		$Accion.start()
		$mood.play("Loading")
		if direction == Vector2.RIGHT:
			direction = Vector2.LEFT
			$Sprite.play("IdleR")
		if direction == Vector2.LEFT:
			direction = Vector2.RIGHT
			$Sprite.play("IdleL")
		if direction == Vector2.UP:
			direction = Vector2.DOWN
			$Sprite.play("IdleU")
		if direction == Vector2.DOWN:
			direction = Vector2.UP
			$Sprite.play("IdleD")
		
		social = 0
	pass # Replace with function body.


func _on_Bano_body_entered(body):
	if (body.name ==  self.name) :
		print("Entro")
		actuando = true;
		$Accion.start(1)
		$mood.play("Loading")
		if direction == Vector2.RIGHT:
			direction = Vector2.LEFT
			$Sprite.play("IdleR")
		if direction == Vector2.LEFT:
			direction = Vector2.RIGHT
			$Sprite.play("IdleL")
		if direction == Vector2.UP:
			direction = Vector2.DOWN
			$Sprite.play("IdleU")
		if direction == Vector2.DOWN:
			direction = Vector2.UP
			$Sprite.play("IdleD")
		
		popo = 0
	pass # Replace with function body.


func _on_Cama_body_entered(body):
	if (body.name ==  self.name) :
		print("Entro")
		actuando = true;
		$Accion.start(1)
		$mood.play("Loading")
		if direction == Vector2.RIGHT:
			direction = Vector2.LEFT
			$Sprite.play("IdleR")
		if direction == Vector2.LEFT:
			direction = Vector2.RIGHT
			$Sprite.play("IdleL")
		if direction == Vector2.UP:
			direction = Vector2.DOWN
			$Sprite.play("IdleU")
		if direction == Vector2.DOWN:
			direction = Vector2.UP
			$Sprite.play("IdleD")
		
		cansado = 0
	pass # Replace with function body.


func _on_BorderL_body_entered(body):
	if (body.name == self.name) :
		if direction == Vector2.RIGHT:
			direction = Vector2.LEFT
			$Sprite.play("IdleR")
		if direction == Vector2.LEFT:
			direction = Vector2.RIGHT
			$Sprite.play("IdleL")
		if direction == Vector2.UP:
			direction = Vector2.DOWN
			$Sprite.play("IdleU")
		if direction == Vector2.DOWN:
			direction = Vector2.UP
			$Sprite.play("IdleD")
	pass # Replace with function body.


func _on_BorderlD_body_entered(body):
	if (body.name == self.name) :
		if direction == Vector2.RIGHT:
			direction = Vector2.LEFT
			$Sprite.play("IdleR")
		if direction == Vector2.LEFT:
			direction = Vector2.RIGHT
			$Sprite.play("IdleL")
		if direction == Vector2.UP:
			direction = Vector2.DOWN
			$Sprite.play("IdleU")
		if direction == Vector2.DOWN:
			direction = Vector2.UP
			$Sprite.play("IdleD")
	pass # Replace with function body.


func _on_BorderlD2_body_entered(body):
	pass # Replace with function body.


func _on_BorderL2_body_entered(body):
	pass # Replace with function body.
