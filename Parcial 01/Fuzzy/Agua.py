import random

def remap (minimo, maximo, temp):
	return  ( 0 + (1 - 0) * ( (temp - minimo)/(maximo - minimo)) )

def GradoInverso (temp, minimo, maximo):
	if temp < minimo:
		return (1)
	elif temp >= minimo and temperatura <= maximo:
		return ( -(temperatura / (maximo - minimo)) + (maximo / (maximo - minimo)) )
	elif temp > maximo:
		return (0)

def Trapezoide (temperatura, minimo_1, minimo_2, maximo_1, maximo_2):
	if temperatura <= minimo_1:
		return (0)
	elif temperatura > minimo_1 and temperatura <= maximo_1:
		return ( (temperatura / (maximo_1 - minimo_1)) - (minimo_1 / (maximo_1 - minimo_1)) )
	elif temperatura > maximo_1 and temperatura <= minimo_2:
		return (1)
	elif temperatura > maximo_2:
		return ( (temperatura / (maximo_2 - minimo_2)) - (maximo_2 / (maximo_2 - minimo_2)) )

def Grado (temperatura, minimo, maximo):
	if temperatura <= minimo:
		return (0)
	elif temperatura > minimo and temperatura <= maximo:
		return ( (temperatura / (maximo-minimo)) - (minimo / (maximo-minimo)) )
	elif temperatura > maximo:
		return (1)

def Bool (Temperatura, minimo):
	if temperatura <= minimo:
		return (0)
	else:
		return(1)

def InverseBool (Temperatura, minimo):
	if temperatura >= minimo:
		return (0)
	else:
		return(1)

random.seed()
temperatura = random.uniform(-10,111)
print("Temperatura Actual: ", temperatura)

temperatura = remap (-10, 110, temperatura)
print ("Remap Temperatura: ", temperatura)

aguahelada = InverseBool(temperatura, 0.1)
print ("Agua Helada: ", aguahelada)

aguafria = GradoInverso(temperatura, 0, 0.4)
print ("Agua Fria: ", aguafria)

aguatibia = Trapezoide(temperatura, 0.2, 0.35, 0.45, 0.6)
print ("Agua Tibia: ", aguatibia)

aguacaliente = Grado(temperatura, 0.4, 1)
print("Agua Caliente: ", aguacaliente)

aguahirviendo = Bool(temperatura, 0.9)
print("Agua Hirviendo: ", aguahirviendo)