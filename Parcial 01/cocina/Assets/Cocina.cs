﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class Cocina : FuzzyLogicBase
{
  public GameObject heat;
  public GameObject time;
  public GameObject grams;
  public GameObject crunchiness;
  
  [Header("Results")]
  public GameObject txtResultInputs;
  public Text txtResult;
  
  void Start(){
    UpdateSlider (heat);
    UpdateSlider (time);
    UpdateSlider (grams);
    UpdateSlider (crunchiness);
  }
  
  public void UpdateSlider(GameObject sliderGroup){
    sliderGroup.transform.Find("txtValue").GetComponent<Text>().text = sliderGroup.GetComponentInChildren<Slider>().value.ToString();
    CalculateFinalResult();
  }
  
  void CalculateFinalResult () {
    float crunchy = BooleanFunc (crunchiness.GetComponentInChildren<Slider>().value, 0.5f);
    float soft = BooleanFunc (crunchiness.GetComponentInChildren<Slider>().value, 1.0f);
    
    float heatCold = TrapezoidalFunc(heat.GetComponentInChildren<Slider>().value, 100, 110, 130, 150);
    float heatMed = TrapezoidalFunc(heat.GetComponentInChildren<Slider>().value, 130, 180, 200, 240);
    float heatMax = DegreeFunc(heat.GetComponentInChildren<Slider>().value, 200, 360);
    
    float timeLittle = TrapezoidalFunc(time.GetComponentInChildren<Slider>().value, 0, 1, 10, 25);
    float timeSome = TrapezoidalFunc(time.GetComponentInChildren<Slider>().value, 10, 30, 35, 45);
    float timeALot = DegreeFunc(time.GetComponentInChildren<Slider>().value, 35, 60);
    
    float gramsSmall = TrapezoidalFunc(grams.GetComponentInChildren<Slider>().value, 0, 0, 100, 150);
    float gramsMedium = TrapezoidalFunc(grams.GetComponentInChildren<Slider>().value, 120, 150, 200, 250);
    float gramsBig = DegreeFunc(grams.GetComponentInChildren<Slider>().value, 200, 300);
    
    txtResultInputs.GetComponent<Text>().text = "CRUNCHY\ncrunchy: " + crunchy + "\nsoft: " + soft
    + "\n\nHEAT\ncold: " + heatCold + "\nmed: " + heatMed + "\nhot: " + heatMax + "\n\nTIME\nlittle: "
    + timeLittle + "\nsome: " + timeSome + "\na lot: " + timeALot
    + "\n\nGRAMS\n small: " + gramsSmall + "\nmedium: " + gramsMedium + "\nbig: " + gramsBig;
    
    float cooked = 0.0f;
    
    if (crunchy == 1){
      cooked = OR( OR( AND( OR(gramsSmall, timeLittle), heatMax),AND (AND (gramsMedium, timeSome), heatMed)), AND ( OR(gramsBig, timeALot), heatCold));
    } else {
      cooked = OR( OR( AND( AND(gramsBig, timeSome), heatMed),AND (AND (gramsMedium, timeLittle), heatMed)), AND ( AND(gramsSmall, timeLittle), heatMed));
    }
    
    string strState = "";
    
    if (cooked == 0 && heat.GetComponentInChildren<Slider>().value >= 240 || cooked == 0 && time.GetComponentInChildren<Slider>().value >= 45 ) {
      strState = "BURNED";
    } else if (cooked == 0 && heat.GetComponentInChildren<Slider>().value <= 130 || cooked == 0 && time.GetComponentInChildren<Slider>().value <= 10){
      strState = "UNDERCOOKED";
    } else if (cooked >= 0.7f) {
      strState = "PERFECT";
    } else if (cooked >= 0.35 && cooked < 0.7f) {
      strState = "GOOD";
    } else {
      strState = "EW";
    }
    
    txtResult.text = "Result: \n" + cooked + " cooked " + strState;
    
  }
  
}
