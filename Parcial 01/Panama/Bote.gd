extends Node2D

enum {
	ESPERANDO,
	SUBIENDO,
	MOVER,
	BAJANDO
}

const velocidad = 100
var estado = MOVER
var previo = null
var direccion = Vector2.RIGHT

func _ready():
	$Timer.start()

func _process(delta):
	match estado:
		ESPERANDO:
			$puerta1.position += Vector2.DOWN * velocidad*delta
		MOVER:
			direccion = Vector2.RIGHT
			mover(delta)
			changestate(ESPERANDO)
		SUBIENDO:
			direccion = Vector2.UP
			mover(delta)
		BAJANDO:
			direccion = Vector2.DOWN
			mover(delta)

func mover(delta):
	$Bote.position += direccion * velocidad * delta

func changestate(nuevoestado):
	estado = nuevoestado
	pass

func _on_Timer_timeout():
	if (estado == ESPERANDO):
		changestate(MOVER)
	$Timer.wait_time= 1.0
	print("Timeout")
	
