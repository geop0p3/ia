﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class Cocina : FuzzyLogicBase
{
  public GameObject ropa;
  public GameObject time;
  public GameObject jabon;
  
  [Header("Results")]
  public GameObject txtResultInputs;
  public Text txtResult;
  
  void Start(){
    UpdateSlider (ropa);
    UpdateSlider (time);
    UpdateSlider (jabon);
  }
  
  public void UpdateSlider(GameObject sliderGroup){
    sliderGroup.transform.Find("txtValue").GetComponent<Text>().text = sliderGroup.GetComponentInChildren<Slider>().value.ToString();
    CalculateFinalResult();
  }
  
  void CalculateFinalResult () {
    
    float jabonFew = TrapezoidalFunc(jabon.GetComponentInChildren<Slider>().value, 0, 1, 3, 5);
    float jabonMed = TrapezoidalFunc(jabon.GetComponentInChildren<Slider>().value, 4, 15, 20, 30);
    float jabonMax = DegreeFunc(jabon.GetComponentInChildren<Slider>().value, 25, 60);
    
    float ropaSome = TrapezoidalFunc(ropa.GetComponentInChildren<Slider>().value, 0, 1, 2, 5);
    float ropaMed = TrapezoidalFunc(ropa.GetComponentInChildren<Slider>().value, 4, 10, 20, 30);
    float ropaAlot = TrapezoidalFunc(ropa.GetComponentInChildren<Slider>().value, 25, 50, 60, 65);
    float ropaToomuch = DegreeFunc(ropa.GetComponentInChildren<Slider>().value, 60, 100);
    
    float timeBarely = TrapezoidalFunc(time.GetComponentInChildren<Slider>().value, 0, 1, 10, 25);
    float timeLittle = TrapezoidalFunc(time.GetComponentInChildren<Slider>().value, 15, 20, 35, 40);
    float timeSome = TrapezoidalFunc(time.GetComponentInChildren<Slider>().value, 38, 40, 45, 55);
    float timeALot = DegreeFunc(time.GetComponentInChildren<Slider>().value, 46, 90);
    
    
    txtResultInputs.GetComponent<Text>().text = "\n\nJabon\nPoco: " + jabonFew + "\nMedio: " + jabonMed + "\nDemasiado: " + jabonMax
    + "\n\nTiempo\nCasi nada: " + timeBarely + "\nPoco: " + timeLittle + "\nMedio: " + timeSome + "\nMucho: " + timeALot
    + "\n\nRopa\n Poca: " + ropaSome + "\nMedia: " + ropaMed + "\nMucha: " + ropaAlot + "\nDemasiado: " + ropaToomuch;
    
    float limpia = 0.0f;
    
    limpia = AND(ropaMed, AND(jabonMed, timeLittle));
    
    string strState = "";
    
    if (limpia == 0 && jabon.GetComponentInChildren<Slider>().value <= 5 || limpia == 0 && time.GetComponentInChildren<Slider>().value <= 20 ) {
      strState = "Sucia";
    } else if (limpia == 0 && jabon.GetComponentInChildren<Slider>().value <= 5 || limpia == 0 && time.GetComponentInChildren<Slider>().value <= 35){
      strState = "Un poco sucia";
    } else if (limpia >= 0.7f) {
      strState = "Muy Limpia";
    } else if (limpia >= 0.35 && limpia < 0.7f) {
      strState = "Limpia";
    } else if (jabon.GetComponentInChildren<Slider>().value >= 40 && limpia == 0){
      strState = "Explosión de Jabón";
    } else if (time.GetComponentInChildren<Slider>().value >= 60 && limpia == 0) {
      strState = "Ropa Maltradada";
    }
    
    txtResult.text = "Result: \n" + limpia + " " + strState;
    
  }
  
}
