﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuzzyLogicBase : MonoBehaviour
{
    public float BooleanFunc (float x, float x0) {
      float result = 0.0f;
      if (x <= x0){
        result = 0;
      } else {
        result = 1;
      }
      
      return result;
    }
    
    public float Inv_BooleanFunc (float x, float x0) {
      float result = 0.0f;
      
      if (x > x0){
        result = 1;
      } else {
        result = 0;
      }
      
      return result;
    }
    
    public float  DegreeFunc (float x, float x0, float x1) {
      float result = 0;
      
      if (x <= x0) {
        result = 0;
      } else if ( x > x0 && x <= x1) {
        result = (x / (x1 - x0)) - (x0 / (x1 - x0));
      } else if (x > x1) {
        result = 1;
      }
      
      return result;
    }
    
    public float Inv_DegreeFunc (float x, float x0, float x1) {
      float result = 0;
      
      if (x < x0) {
        result = 1;
      } else if ( x > x0 && x <= x1) {
        result = -(x / (x1 - x0)) + (x0 / (x1 - x0));
      } else if (x > x1) {
        result = 0;
      }
      
      return result;
    }
    
    public float TriangleFunc (float x, float x0, float x1, float x2) {
      float result = 0;
      
      if(x <= x0){
        result = 0;
      } else if (x > x0 && x <= x1) {
        result = (x / (x1 - x0)) - (x0 / (x1 - x0)); 
      } else if (x > x1 && x <= x2) {
        result = - (x / (x2 - x1)) + (x2 / (x2 - x1));
      } else if (x > x2){
        result = 1;
      }
      
      return result;
    }
    
    public float TrapezoidalFunc (float x, float x0, float x1, float x2, float x3) {
      float result = 0;
      
      if(x <= x0){
        result = 0;
      } else if (x > x0 && x <= x1) {
        result = (x / (x1 - x0)) - (x0 / (x1 - x0)); 
      } else if (x > x1 && x <= x2) {
        result = 1.0f;
      } else if (x > x2 && x <= x3){
        result = - (x / (x3 - x2)) + (x3 / (x3 - x2));
      } else if (x > x3) {
        result = 0;
      }
      
      return result;
    }
    
    public float AND (float a, float b) {
      return Mathf.Min(a, b);
    }
    
    public float OR(float a, float b) {
      return Mathf.Max(a, b);
    }
    
    public float NOT(float a){
      return 1.0f - a;
    }
}
