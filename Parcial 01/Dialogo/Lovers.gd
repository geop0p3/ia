extends Node2D

var respuesta = null
var previo = null;
var llave = 0;
var fail = false;
var scroll = false;



func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "reveal" && fail == false):
		scroll = false;
		$Afirmar/AnimationPlayer.play("Moveup")
		$Negar/AnimationPlayer.play("move")
	if(anim_name == "clear"):
		if(respuesta == true && previo == null):
			$Panel/TExto.text = "Perfecto! podremos ser amigos! Me caes bien y dime, ¿Te gustan las carnitas?"
			$Afirmar.text = "Uuuuf si!"
			$Negar.text = "Estoy a dieta"
		if(respuesta == true && previo == true && llave == 2):
			$Panel/TExto.text = "Ay que rico! Le pones cebolla?"
			$Afirmar.text = "Un chorro!!"
			$Negar.text = "Estas loco?!"
		if(respuesta == false && previo == true && llave == 2):
			$Panel/TExto.text = "Chales men y los tacos al pastor?"
			$Afirmar.text = "Claro!"
			$Negar.text = "Nel pastel"
		if(respuesta == true && previo == true && llave == 3):
			fail = true
			$Panel/TExto.text = "Wooow entonces te rifas, aunque seguro te huele la boca..."
			$Timer.start()
		if(respuesta == false && previo == true && llave == 3):
			fail = true
			$Panel/TExto.text = "Que burrazo, la cebolla le da mucho sabor al taco..."
			$Timer.start()
		if(respuesta == true && previo == false && llave == 3):
			fail = true
			$Panel/TExto.text = "Vamos por unos! Yo invito. Venden unas quesadillas increibles..."
			$Timer.start()
		if(respuesta == false && previo == false && llave == 3):
			fail = true
			$Panel/TExto.text = "No manches men, no comes nada rico. Te pasas de lanza. Bye..."
			$Timer.start()
		if (respuesta == false && previo == null):
			$Panel/TExto.text = "Chales broder! Que mala onda.... Esta seguro que no quieres aprender mas de los furros locos jeje XD"
			$Afirmar.text = "Bueno pues si"
			$Negar.text = "QUE NO!!"
		if(respuesta == false && previo == false && llave == 2):
			fail= true
			$Panel/TExto.text = "Mtaaaa, bueno entonces abrete prro!!!"
			$Timer.start()
		if(respuesta == true && previo == false && llave == 2):
			fail = true;
			$Panel/TExto.text = "Excelente, vente, te mostrare nuestro club..."
			$Timer.start()
		$Panel/TExto/AnimationPlayer.play("reveal")

func _on_Afirmar_pressed():
	$Click.play()
	previo = respuesta;
	respuesta = true;
	llave +=1
	$Afirmar/AnimationPlayer.play_backwards("Moveup")
	$Negar/AnimationPlayer.play_backwards("move")
	$Panel/TExto/AnimationPlayer.play("clear")


func _on_Negar_pressed():
	$Click.play()
	previo = respuesta
	respuesta = false;
	llave +=1
	$Afirmar/AnimationPlayer.play_backwards("Moveup")
	$Negar/AnimationPlayer.play_backwards("move")
	$Panel/TExto/AnimationPlayer.play("clear")


func _on_Timer_timeout():
	get_tree().quit()


func _on_Negar_mouse_entered():
	$Hover.play()


func _on_Afirmar_mouse_entered():
	$Hover.play()
	pass # Replace with function body.


func _on_AnimationPlayer_animation_started(anim_name):
	if (anim_name == "reveal"):
		scroll = true;
	pass # Replace with function body.
