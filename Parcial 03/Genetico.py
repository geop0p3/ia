import statistics
import random

def generarPadre(tamano, setGenes, obtenerRank):
    genes = []
    while len(genes) < tamano:
        poblacion = min(tamano - len(genes), len(setGenes))
        genes.extend(random.sample(setGenes, poblacion))
    genes = ''.join(genes)
    rank = obtenerRank(genes)
    return Cromosoma(genes, rank)

def mutar(padre, setGenes, obtenerRank):
    index = random.randrange(0, len(padre.Genes))
    hijos = list(padre.Genes)
    nuevoGen, alterno = random.sample(setGenes, 2)
    hijos[index] = alterno if nuevoGen == hijos[index] else nuevoGen
    genes = ''.join(hijos)
    rank = obtenerRank(genes)
    return Cromosoma(genes, rank)

def mejorGen(obtenerRank, tamanoObj, rankMejor, setGenes, imprimir):
    random.seed()
    bestpadre = generarPadre(tamanoObj, setGenes, obtenerRank)
    imprimir(bestpadre)
    if bestpadre.rank >= rankMejor:
        return bestpadre
    while True:
        hijo = mutar(bestpadre, setGenes, obtenerRank)
        if bestpadre.rank >= hijo.rank:
            continue
        imprimir(hijo)
        if hijo.rank >= rankMejor:
            return hijo
        bestpadre = hijo

class Cromosoma:
    def __init__(self, genes, rank):
        self.Genes = genes
        self.rank = rank

def obtenerRank(inicio, objetivo):
    return sum(1 for expected, actual in zip(objetivo, inicio) if expected == actual)

def imprimir(candidato):
    print("{}\t{}".format(candidato.Genes, candidato.rank))

class Colores():
    genes = "0123456789ABCDEF"

    def encontrarColor(self, objetivo):

        def fnGetrank(genes):
            return obtenerRank(genes, objetivo)

        def fnimprimir(candidato):
            imprimir(candidato)

        rankMejor = len(objetivo)
        best = mejorGen(fnGetrank, len(objetivo), rankMejor, self.genes, fnimprimir)

def main():
    Colores.encontrarColor(Colores, "00BFFF")
    print("------------")
    Colores.encontrarColor(Colores, "E9CDF6")

if __name__ == '__main__':
    main()
