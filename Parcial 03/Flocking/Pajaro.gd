extends Node2D

var ubicacion
var velocidad
var aceleracion
var tamano
var velocidad_maxima = 10
var instancia
var Objetivo
var ruido = OpenSimplexNoise.new()
var contador
var contador2
var espacio = 50
var margen = 50

func _ready():
	randomize()
	ruido.seed = randi()
	contador = randf()
	contador2 = randf()
	Objetivo = preload("res://Objetivo.tscn")
	instancia = Objetivo.instance()
	aceleracion = Vector2(0,0)
	velocidad = Vector2(rand_range(-10,10),rand_range(-10,10))
	tamano = velocidad.length()
	ubicacion = Vector2(250,250)

func _process(delta):
	velocidad = velocidad + aceleracion
	tamano = velocidad.length()
	if(velocidad.length()>velocidad_maxima):
		velocidad = velocidad.normalized()*velocidad_maxima
	ubicacion = ubicacion + velocidad
	position = ubicacion
	aceleracion = aceleracion * 0
	rotation = velocidad.angle()
	
	if rotation > PI/2 || rotation < -PI/2:
		get_node( "Sprite" ).set_flip_v( true )
	else:
		get_node( "Sprite" ).set_flip_v( false )
	
	if ubicacion.x > get_viewport().size.x + margen:
		ubicacion.x = -margen
	elif ubicacion.x < 0 - margen:
		ubicacion.x = get_viewport().size.x + margen
	if ubicacion.y > get_viewport().size.y + margen:
		ubicacion.y = -margen
	elif ubicacion.y < 0 - margen:
		ubicacion.y = get_viewport().size.y + margen
	
	aplicarFuerza(Vector2(ruido.get_noise_2d(contador,contador),ruido.get_noise_2d(contador2,contador2))*.2)
	contador = contador + delta * 2
	contador2 = contador2 + delta * 2

func aplicarFuerza(fuerza):
	aceleracion += fuerza

func buscar(pajaro):
	instancia.objetivo = pajaro.ubicacion - ubicacion
	instancia.objetivo = instancia.objetivo * .02
	instancia.linear = instancia.objetivo - velocidad
	instancia.linear = instancia.linear * .05
	aplicarFuerza(instancia.linear)

func buscar_vector(pajaro):
	var objetivo = pajaro - ubicacion
	objetivo = objetivo * .02
	var linear = instancia.objetivo - velocidad
	linear = linear * .1
	aplicarFuerza(linear)

func separar(pajaros):
	var vector = Vector2(0,0)
	var contador = 0
	for pajaro in pajaros :
		if self != pajaro && ubicacion.distance_to(pajaro.ubicacion)<50*espacio: 
			vector += (ubicacion - pajaro.ubicacion).normalized()
			contador += 1
	if contador > 0:
		vector = vector / contador
		vector = vector-velocidad
		if vector.length() > velocidad_maxima:
			vector = vector.normalized() * velocidad_maxima
		aplicarFuerza(vector*.03)

func cohesion(pajaros):
	var vector = Vector2(0,0)
	var contador = 0
	for pajaro in pajaros :
		if self != pajaro && ubicacion.distance_to(pajaro.ubicacion)<75*espacio:
			vector += pajaro.ubicacion
			contador += 1
	if contador > 0:
		vector = vector / contador
		buscar_vector(vector)

func alinear(pajaros):
	var vector = Vector2(0,0)
	var contador = 0
	for pajaro in pajaros :
		if self != pajaro && ubicacion.distance_to(pajaro.ubicacion)<70*espacio:
			vector += pajaro.velocidad
			contador += 1
	if contador > 0:
		vector = vector / contador
		vector.normalized()
		vector = vector * velocidad_maxima
		vector = vector - velocidad
		if vector.length() > velocidad_maxima:
			vector = vector.normalized() * velocidad_maxima
		aplicarFuerza(vector*.025)