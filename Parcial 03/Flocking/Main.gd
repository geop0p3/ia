extends Node2D

var Pajaro
var Pajaros = []

func _ready():
	Pajaro = preload("res://Pajaro.tscn")
	for i in range(15):
		Pajaros.append(Pajaro.instance())
	for pajaro in Pajaros:
		add_child(pajaro)

func _process(delta):
	for pajaro in Pajaros:
		pajaro.separar(Pajaros)
		pajaro.cohesion(Pajaros)
		pajaro.alinear(Pajaros)
