extends Node2D

var contador = 0
var resultados = []
var experto = 0
var randomPlayer = 0
var winsExpert = 0
var winsRandom = 0
var draws = 0
var container = null
onready var empateTijeras = preload("res://EmpateTijeras.tscn")
onready var empatePapel = preload("res://Empate Papel.tscn")
onready var empatePiedras = preload("res://Empate Piedras.tscn")

onready var perdioTijeras = preload("res://PerdioTijeras.tscn")
onready var perdioPapel = preload("res://PerdioPapel.tscn")
onready var perdioPiedra = preload("res://PerdioPiedra.tscn")

onready var ganoTijeras = preload("res://GanoTijeras.tscn")
onready var ganoPapel = preload("res://GanoPapel.tscn")
onready var ganoPiedra = preload("res://GanoPiedra.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	container = $ScrollContainer/HBoxContainer
	turno()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func turno():
	if contador > 0:
		experto = $SistemaExperto.pensar()
	else :
		var new = [0,1,2]
		experto = $SistemaExperto.randomchoice(new)
	randomPlayer = $RandomPLayer.randomchoice()
	ganador(experto, randomPlayer)
	contador += 1
	$Timer.start()

func _process(delta):
	$Label3/WinsExpert.text = str(winsExpert)
	$Label4/WinsRandom.text = str(winsRandom)
	$Label5/Draws.text = str(draws)

func _on_Timer_timeout():
	turno()
	pass # Replace with function body.

func ganador(experto, randomPlayer):
	if (experto == 0 and experto == randomPlayer):
		draws += 1
		var panel = empatePiedras.instance()
		container.add_child(panel)
		resultados.append("Empate Piedra")
	if (experto == 0 and randomPlayer == 1):
		winsRandom += 1
		var panel = perdioPiedra.instance()
		container.add_child(panel)
		resultados.append("Perdio Experto Piedra vs Papel")
	if (experto == 0 and randomPlayer == 2):
		winsExpert += 1
		var panel = ganoPiedra.instance()
		container.add_child(panel)
		resultados.append("Gano Experto Piedra vs Tijera")
	if (experto == 1 and experto == randomPlayer):
		draws += 1
		var panel = empatePapel.instance()
		container.add_child(panel)
		resultados.append("Empate Papel")
	if (experto == 1 and randomPlayer == 0):
		winsExpert += 1
		var panel = ganoPapel.instance()
		container.add_child(panel)
		resultados.append("Gano Experto Papel vs Piedra")
	if (experto == 1 and randomPlayer == 2):
		winsRandom += 1
		var panel = perdioPapel.instance()
		container.add_child(panel)
		resultados.append("Perdio Experto Papel vs Tijera")
	if (experto == 2 and experto == randomPlayer):
		draws += 1
		var panel = empateTijeras.instance()
		container.add_child(panel)
		resultados.append("Empate Tijeras")
	if (experto == 2 and randomPlayer == 0):
		winsRandom += 1
		var panel = perdioTijeras.instance()
		container.add_child(panel)
		resultados.append("Perdio Experto Tijera vs Piedra")
	if (experto == 2 and randomPlayer == 1):
		winsExpert += 1
		var panel = ganoTijeras.instance()
		container.add_child(panel)
		resultados.append("Gano Experto Tijera vs Papel")
	
	
	
	
	
	