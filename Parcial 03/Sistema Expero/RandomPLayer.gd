extends Node2D

var papel = preload("res://Sprites/red.png")
var tijera = preload("res://Sprites/red2.png")
var piedra = preload("res://Sprites/red1.png")
var movimientos = []

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	pass # Replace with function body.

func randomchoice():
	var nums = [0,1,2]
	var choice = nums[randi() % nums.size()]
	if choice == 0:
		$Sprite.set_texture(piedra)
	if choice == 1:
		$Sprite.set_texture(papel)
	if choice == 2:
		$Sprite.set_texture(tijera)
	movimientos.append(choice)
	return choice
