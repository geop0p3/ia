extends Node2D

var papelSprite = preload("res://Sprites/purple.png")
var tijeraSprite = preload("res://Sprites/purple2.png")
var piedraSprite = preload("res://Sprites/purple1.png")
var movimientos = []
onready var movimientosRandom = get_parent().get_child(1).movimientos
onready var resultados = get_parent().resultados

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	pass # Replace with function body.

func randomchoice(arreglo):
	var nums = arreglo
	var choice = nums[randi() % nums.size()]
	if choice == 0:
		$Sprite.set_texture(piedraSprite)
	if choice == 1:
		$Sprite.set_texture(papelSprite)
	if choice == 2:
		$Sprite.set_texture(tijeraSprite)
	movimientos.append(choice)
	return choice

func pensar():
	var ganoTijeras = resultados.count("Gano Experto Tijera vs Papel")
	var ganoPiedra = resultados.count("Gano Experto Piedra vs Tijera")
	var ganoPapel = resultados.count("Gano Experto Papel vs Piedra")
	
	var movimientosTijera = movimientos.count(2)
	var movimientosPiedra = movimientos.count(0)
	var movimientosPapel = movimientos.count(1)
	
	var probabilidadPiedra = 0;
	var probabilidadPapel = 0
	var probabilidadTijera = 0
	
	if (movimientosTijera > 0 and ganoTijeras > 0):
		probabilidadTijera = float (ganoTijeras) / movimientosTijera
	
	if (movimientosPiedra > 0 and ganoPiedra > 0):
		probabilidadPiedra = float (ganoPiedra) / movimientosPiedra
		
	if (movimientosPapel > 0 and ganoPapel > 0):
		probabilidadPapel = float (ganoPapel) / movimientosPapel
		
	
	if (probabilidadPapel > probabilidadPiedra and probabilidadPapel > probabilidadTijera):
		if (movimientosRandom.back() == 2):
			var new = [2,0]
			return randomchoice(new)
		print("Return papel")
		$Sprite.set_texture(papelSprite)
		var choice = 1
		movimientos.append(choice)
		return choice
	elif (probabilidadPiedra > probabilidadPapel and probabilidadPiedra > probabilidadTijera):
		if (movimientosRandom.back() == 1 ):
			var new = [1, 2]
			return randomchoice(new)
		print ("return piedra")
		$Sprite.set_texture(piedraSprite)
		var choice = 0
		movimientos.append(choice)
		return choice
	elif (probabilidadTijera > probabilidadPapel and probabilidadTijera > probabilidadPiedra):
		if (movimientosRandom.back() == 0):
			var new = [0,1]
			return randomchoice(new)
		print ("return tijeras")
		$Sprite.set_texture(tijeraSprite)
		var choice = 2
		movimientos.append(choice)
		return choice
	else:
		print ("return random")
		var new = [0,1,2]
		return randomchoice(new)
	pass