extends Spatial

var ubicacion
var velocidad
var aceleracion
var tamano
var velocidad_maxima = 0.2
var instancia
var Objetivo
var ruido = OpenSimplexNoise.new()
var contador
var contador2
var contador3
var espacio = 0.01
var margen = 2

func _ready():
	randomize()
	ruido.seed = randi()
	contador = randf()
	contador2 = randf()
	contador3 = randf()
	Objetivo = preload("res://Objetivo.tscn")
	instancia = Objetivo.instance()
	aceleracion = Vector3(0,0,0)
	velocidad = Vector3(rand_range(-1,1),rand_range(-1,1),rand_range(-1,1))
	tamano = velocidad.length()
	ubicacion = Vector3(0,0,0)

func _process(delta):
	velocidad = velocidad + aceleracion
	tamano = velocidad.length()
	if(velocidad.length()>velocidad_maxima):
		velocidad = velocidad.normalized()*velocidad_maxima
	ubicacion = ubicacion + velocidad
	translation = ubicacion
	aceleracion = aceleracion * 0.2
	rotation = Vector3(1,1,1)
	
	if ubicacion.x > 30:
		velocidad.x *-1
	elif ubicacion.x < -30:
		velocidad.x *-1
	if ubicacion.y > 15:
		velocidad.y *-1
	elif ubicacion.y < 0:
		velocidad.y *-1
	if ubicacion.z > 15:
		velocidad.z *-1
	elif ubicacion.z < -15:
		velocidad.z *-1
	
	aplicarFuerza(Vector3(ruido.get_noise_2d(contador, contador), ruido.get_noise_2d(contador2, contador2), ruido.get_noise_2d(contador3, contador3))*.2)
	contador = contador + delta * 2
	contador2 = contador2 + delta * 2
	contador3 = contador3 + delta * 3

func aplicarFuerza(fuerza):
	aceleracion += fuerza

func buscar(pajaro):
	instancia.objetivo = pajaro.ubicacion - ubicacion
	instancia.objetivo = instancia.objetivo * .02
	instancia.linear = instancia.objetivo - velocidad
	instancia.linear = instancia.linear * .05
	aplicarFuerza(instancia.linear)

func buscar_vector(pajaro):
	var objetivo = pajaro - ubicacion
	objetivo = objetivo * .02
	var linear = instancia.objetivo - velocidad
	linear = linear * .1
	aplicarFuerza(linear)

func separar(pajaros):
	var vector = Vector3(0,0,0)
	var contador = 0
	for pajaro in pajaros :
		if self != pajaro && ubicacion.distance_to(pajaro.ubicacion)<50*espacio: 
			vector += (ubicacion - pajaro.ubicacion).normalized()
			contador += 1
	if contador > 0:
		vector = vector / contador
		vector = vector-velocidad
		if vector.length() > velocidad_maxima:
			vector = vector.normalized() * velocidad_maxima
		aplicarFuerza(vector*.03)

func cohesion(pajaros):
	var vector = Vector3(0,0,0)
	var contador = 0
	for pajaro in pajaros :
		if self != pajaro && ubicacion.distance_to(pajaro.ubicacion)<75*espacio:
			vector += pajaro.ubicacion
			contador += 1
	if contador > 0:
		vector = vector / contador
		buscar_vector(vector)

func alinear(pajaros):
	var vector = Vector3(0,0,0)
	var contador = 0
	for pajaro in pajaros :
		if self != pajaro && ubicacion.distance_to(pajaro.ubicacion)<70*espacio:
			vector += pajaro.velocidad
			contador += 1
	if contador > 0:
		vector = vector / contador
		vector.normalized()
		vector = vector * velocidad_maxima
		vector = vector - velocidad
		if vector.length() > velocidad_maxima:
			vector = vector.normalized() * velocidad_maxima
		aplicarFuerza(vector*.025)