extends Spatial

var Pajaro
var Pajaros = []

func _ready():
	Pajaro = preload("res://pez.tscn")
	for i in range(30):
		Pajaros.append(Pajaro.instance())
	for pajaro in Pajaros:
		add_child(pajaro)

func _process(delta):
	for pajaro in Pajaros:
		pajaro.separar(Pajaros)
		pajaro.cohesion(Pajaros)
		pajaro.alinear(Pajaros)
