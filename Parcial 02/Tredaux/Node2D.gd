extends Node2D
var arreglo = null
onready var tile = get_child(1)
onready var player = get_child(3)
var previo = []
var listo = false
var terminado = false
var inicio = Vector2(0,1)

func _ready():
	arreglo = load_file("nivel.txt")
	despliegue(arreglo)
	player.position = tile.map_to_world(inicio) + Vector2(32,32)

func _process(delta):
	if listo:
		navegar()

func despliegue(arreglo):
	var i = 0
	while i < arreglo.size():
		var j = 0
		while j < arreglo[0].length():
			var string = arreglo[i]
			tile.set_cell(j, i, int(string[j]))
			j += 1
		i += 1
	listo = true

func load_file(filename):
	var result = []
	var f = File.new()
	f.open(filename, File.READ)
	var index = 1
	while not f.eof_reached():
		var line = f.get_line()
		result.append(line)
		index += 1
	f.close()
	return result
	
func navegar():
	if terminado == false:
		if tile.get_cell(inicio.x, inicio.y - 1) == 1 and tile.get_cell(inicio.x, inicio.y - 1) != -1 : 
			print("Arriba")
			previo.append(inicio)
			inicio = inicio + Vector2(0, -1)
			player.position = tile.map_to_world(inicio) + Vector2(32,32)
			tile.set_cellv(inicio, 4)
			print (inicio)
		elif tile.get_cell(inicio.x -1, inicio.y) == 1 and tile.get_cell(inicio.x -1, inicio.y) != -1:
			if tile.get_cell(inicio.x -1, inicio.y) == 3:
				inicio = inicio + Vector2(- 1, 0)
				player.position = tile.map_to_world(inicio) + Vector2(32,32)
			print("Left")
			previo.append(inicio)
			inicio = inicio + Vector2(- 1, 0)
			player.position = tile.map_to_world(inicio) + Vector2(32,32)
			tile.set_cellv(inicio, 4)
			print (inicio)
		elif tile.get_cell(inicio.x +1, inicio.y) == 1 and tile.get_cell(inicio.x +1, inicio.y) != -1:
			if tile.get_cell(inicio.x, inicio.y -1) == 3:
				inicio = inicio + Vector2(1, 0)
				player.position = tile.map_to_world(inicio) + Vector2(32,32)
			print("Right")
			previo.append(inicio)
			inicio = inicio + Vector2(1, 0)
			player.position = tile.map_to_world(inicio) + Vector2(32,32)
			tile.set_cellv(inicio, 4)
			print (inicio)
		elif tile.get_cell(inicio.x, inicio.y +1) == 1 and tile.get_cell(inicio.x, inicio.y +1) != -1:
			if tile.get_cell(inicio.x, inicio.y +1) == 3:
				terminado = true
				inicio = inicio + Vector2(0, 1)
			player.position = tile.map_to_world(inicio) + Vector2(32,32)
			print("Down")
			previo.append(inicio)
			inicio = inicio + Vector2(0, 1)
			player.position = tile.map_to_world(inicio) + Vector2(32,32)
			tile.set_cellv(inicio, 4)
			print (inicio)
		elif tile.get_cell(inicio.x, inicio.y -1) == 3 or tile.get_cell(inicio.x+1, inicio.y) == 3 or tile.get_cell(inicio.x-1, inicio.y) == 3:
			terminado = true;
		else:
			player.position = tile.map_to_world(previo.back( )) + Vector2(32,32)
			inicio = previo.back()
			previo.pop_back()
			pass