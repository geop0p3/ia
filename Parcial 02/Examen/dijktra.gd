extends Node2D
var grafo = {
'Arad':{'Zerind':75,'Sibiu':140,'Timisoara':118},
'Zerind':{'Arad':75,'Oradera':71},
'Oradera':{'Zerind':71,'Sibiu':151},
'Timisoara':{'Arad':118, 'Lugoj':111},
'Lugoj':{'Timisoara':111, 'Mehadia':70},
'Sibiu':{'Arad':140, 'Oradera': 151, 'Fagaras':99, 'Rimnicu':80},
'Mehadia':{'Lugoj':70, 'Drubeta':75},
'Drubeta':{'Mehadia':75, 'Craiova':120},
'Craiova':{'Drubeta':120, 'Rimnicu': 146, 'Pitesti': 138},
'Rimnicu':{'Sibiu': 80, 'Pitesti': 97, 'Craiova': 146},
'Pitesti':{'Rimnicu': 97, 'Craiova': 138, 'Bucarest':101},
'Bucarest':{'Fagaras': 211, 'Giurgiu':90, 'Urzieeni': 85, 'Pitesti': 101},
'Fagaras':{'Sibiu':99, 'Bucarest': 211},
'Giurgiu':{'Bucarest': 90},
'Urzieeni':{'Bucarest': 85, 'Hirsova':98, 'Vaslui':142},
'Hirsova':{'Urzieeni': 98, 'Eforie':86},
'Eforie':{'Hirsova': 86},
'Vaslui':{'Urzieeni': 142, 'Iasi':92},
'Iasi':{'Vaslui': 92, 'Neamt': 87},
'Neamt':{'Iasi':87},
}

var heuristica = {
	'Arad': 366,
	'Zerind': 374,
	'Oradera': 380,
	'Timisoara': 329,
	'Lugoj': 244,
	'Sibiu': 253,
	'Mehadia': 241,
	'Drubeta': 242,
	'Craiova': 160,
	'Rimnicu': 193,
	'Pitesti': 100,
	'Bucarest': 0,
	'Fagaras': 176,
	'Giurgiu': 77,
	'Urzieeni': 80,
	'Hirsova': 151,
	'Eforie': 161,
	'Vaslui': 199,
	'Iasi': 226,
	'Neamt': 234
}

var blueCheck = preload("res://Ui/blue_boxTick.png")
var greenCheck = preload("res://Ui/green_boxTick.png")
var greyCheck = preload("res://Ui/grey_boxTick.png")
var default = preload("res://Ui/grey_circle.png")
var clicks = 0;
var metaSelect = null;
var inicioSelect = null;
var terminado = false;
var camino = []

func _input(event):
	if event.is_action_pressed("click"):
		if clicks == 2:
			print("Clearing Clicks")
			clicks = 0
			get_node(inicioSelect).icon = default
			get_node(metaSelect).icon = default
		if terminado == true:
			get_tree().reload_current_scene()

func dijkstra(grafo,inicio,meta):
	var distanciaCorta = {}
	var previo = {}
	var porRevisar = grafo
	var infinito = 99999999999999999
	for nodo in porRevisar:
		distanciaCorta[nodo] = infinito
		distanciaCorta[inicio] = 0
	while porRevisar:
		var mejorNodo = null
		for nodo in porRevisar:
			if mejorNodo == null:
				mejorNodo = nodo
			elif distanciaCorta[nodo] < distanciaCorta[mejorNodo]:
				mejorNodo = nodo
		for hijo in grafo[mejorNodo].keys():
			if hijo != "h":
				for peso in grafo[mejorNodo].values():
					if peso + distanciaCorta[mejorNodo] < distanciaCorta[hijo]:
						distanciaCorta[hijo] = peso + distanciaCorta[mejorNodo]
						previo[hijo] = mejorNodo
		porRevisar.erase(mejorNodo)
	var actual = meta
	while actual != inicio:
		camino.insert(0,actual)
		actual = previo.get(actual)
	camino.insert(0,inicio)
	if distanciaCorta[meta] != infinito:
		terminado = true
		print('La distancia es ' + str(distanciaCorta[meta]))
		print('El camino es ' + str(camino))
		for i in range(0, camino.size()):
			get_node(camino[i]).icon=greyCheck
		get_node(meta).icon = greenCheck
		get_node(inicio).icon = blueCheck

func aestrella(grafo,heuristica,inicio,meta):
	var abierto = [[inicio,heuristica[inicio]]]
	var cerrado = []
	var costo = []
	var valores = []
	var fn = null
	while abierto.empty() !=true:
		print("Entro while")
		for i in range(0, abierto.size()):
			fn = [abierto[i][1]]
		var eleccion = fn.find(fn.min())
		var nodo = abierto[eleccion][0]
		cerrado.push_back(abierto[eleccion])
		abierto.remove(eleccion)
		if cerrado[-1][0] == meta:
			break
		for hijo in grafo[nodo]:
#			if cerrado.find(hijo):
#				continue
			valores = grafo[nodo].values()
			print(valores)
			var fn_nodo = valores[hijo] + heuristica[hijo]
			camino.push_back(hijo, fn_nodo)
			abierto.push_back(hijo, fn_nodo)
	pass

func _on_Arad_pressed():
	if clicks == 0:
		$Arad.icon = blueCheck
		inicioSelect = $Arad.name
	if clicks == 1:
		$Arad.icon = greenCheck
		metaSelect = $Arad.name
	if clicks == 2:
		clicks = 0
		$Arad.icon = default
	clicks += 1
	pass # Replace with function body.

func _on_Timisoara_pressed():
	if clicks == 0:
		$Timisoara.icon = blueCheck
		inicioSelect = $Timisoara.name
	if clicks == 1:
		$Timisoara.icon = greenCheck
		metaSelect = $Timisoara.name
	if clicks == 2:
		clicks = 0
		$Timisoara.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Lugoj_pressed():
	if clicks == 0:
		$Lugoj.icon = blueCheck
		inicioSelect = $Lugoj.name
	if clicks == 1:
		$Lugoj.icon = greenCheck
		metaSelect = $Lugoj.name
	if clicks == 2:
		clicks = 0
		$Lugoj.icon = default
	clicks += 1
	pass # Replace with function body.

func _on_Dijkstra_pressed():
	dijkstra(grafo, inicioSelect, metaSelect)
	pass # Replace with function body.



func _on_Sibiu_pressed():
	if clicks == 0:
		$Sibiu.icon = blueCheck
		inicioSelect = $Sibiu.name
	if clicks == 1:
		$Sibiu.icon = greenCheck
		metaSelect = $Sibiu.name
	if clicks == 2:
		clicks = 0
		$Sibiu.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Fagaras_pressed():
	if clicks == 0:
		$Fagaras.icon = blueCheck
		inicioSelect = $Fagaras.name
	if clicks == 1:
		$Fagaras.icon = greenCheck
		metaSelect = $Fagaras.name
	if clicks == 2:
		clicks = 0
		$Fagaras.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Mehadia_pressed():
	if clicks == 0:
		$Mehadia.icon = blueCheck
		inicioSelect = $Mehadia.name
	if clicks == 1:
		$Mehadia.icon = greenCheck
		metaSelect = $Mehadia.name
	if clicks == 2:
		clicks = 0
		$Mehadia.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Drubeta_pressed():
	if clicks == 0:
		$Drubeta.icon = blueCheck
		inicioSelect = $Drubeta.name
	if clicks == 1:
		$Drubeta.icon = greenCheck
		metaSelect = $Drubeta.name
	if clicks == 2:
		clicks = 0
		$Drubeta.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Craiova_pressed():
	if clicks == 0:
		$Craiova.icon = blueCheck
		inicioSelect = $Craiova.name
	if clicks == 1:
		$Craiova.icon = greenCheck
		metaSelect = $Craiova.name
	if clicks == 2:
		clicks = 0
		$Craiova.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Pitesti_pressed():
	if clicks == 0:
		$Pitesti.icon = blueCheck
		inicioSelect = $Pitesti.name
	if clicks == 1:
		$Pitesti.icon = greenCheck
		metaSelect = $Pitesti.name
	if clicks == 2:
		clicks = 0
		$Pitesti.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Bucarest_pressed():
	if clicks == 0:
		$Bucarest.icon = blueCheck
		inicioSelect = $Bucarest.name
	if clicks == 1:
		$Bucarest.icon = greenCheck
		metaSelect = $Bucarest.name
	if clicks == 2:
		clicks = 0
		$Bucarest.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Giurgiu_pressed():
	if clicks == 0:
		$Giurgiu.icon = blueCheck
		inicioSelect = $Giurgiu.name
	if clicks == 1:
		$Giurgiu.icon = greenCheck
		metaSelect = $Giurgiu.name
	if clicks == 2:
		clicks = 0
		$Giurgiu.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Urzieeni_pressed():
	if clicks == 0:
		$Urzieeni.icon = blueCheck
		inicioSelect = $Urzieeni.name
	if clicks == 1:
		$Urzieeni.icon = greenCheck
		metaSelect = $Urzieeni.name
	if clicks == 2:
		clicks = 0
		$Urzieeni.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Vaslui_pressed():
	if clicks == 0:
		$Vaslui.icon = blueCheck
		inicioSelect = $Vaslui.name
	if clicks == 1:
		$Vaslui.icon = greenCheck
		metaSelect = $Vaslui.name
	if clicks == 2:
		clicks = 0
		$Vaslui.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Iasi_pressed():
	if clicks == 0:
		$Iasi.icon = blueCheck
		inicioSelect = $Iasi.name
	if clicks == 1:
		$Iasi.icon = greenCheck
		metaSelect = $Iasi.name
	if clicks == 2:
		clicks = 0
		$Iasi.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Neamt_pressed():
	if clicks == 0:
		$Neamt.icon = blueCheck
		inicioSelect = $Neamt.name
	if clicks == 1:
		$Neamt.icon = greenCheck
		metaSelect = $Neamt.name
	if clicks == 2:
		clicks = 0
		$Neamt.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Hirsova_pressed():
	if clicks == 0:
		$Hirsova.icon = blueCheck
		inicioSelect = $Hirsova.name
	if clicks == 1:
		$Hirsova.icon = greenCheck
		metaSelect = $Hirsova.name
	if clicks == 2:
		clicks = 0
		$Hirsova.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Eforie_pressed():
	if clicks == 0:
		$Eforie.icon = blueCheck
		inicioSelect = $Eforie.name
	if clicks == 1:
		$Eforie.icon = greenCheck
		metaSelect = $Eforie.name
	if clicks == 2:
		clicks = 0
		$Eforie.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Rimnicu_pressed():
	if clicks == 0:
		$Rimnicu.icon = blueCheck
		inicioSelect = $Rimnicu.name
	if clicks == 1:
		$Rimnicu.icon = greenCheck
		metaSelect = $Rimnicu.name
	if clicks == 2:
		clicks = 0
		$Rimnicu.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Zerind_pressed():
	if clicks == 0:
		$Zerind.icon = blueCheck
		inicioSelect = $Zerind.name
	if clicks == 1:
		$Zerind.icon = greenCheck
		metaSelect = $Zerind.name
	if clicks == 2:
		clicks = 0
		$Zerind.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_Oradera_pressed():
	if clicks == 0:
		$Oradera.icon = blueCheck
		inicioSelect = $Oradera.name
	if clicks == 1:
		$Oradera.icon = greenCheck
		metaSelect = $Oradera.name
	if clicks == 2:
		clicks = 0
		$Oradera.icon = default
	clicks += 1
	pass # Replace with function body.


func _on_AEstrella_pressed():
	aestrella(grafo,heuristica,inicioSelect,metaSelect)
	pass # Replace with function body.
