extends Node2D
var grafo = {
's':{'b':7,'a':2},
'b':{'a':2,'c':1},
'a':{'b':3,'c':8,'d':5},
'c':{'d':4},
'd':{'c':5}
}

func _ready():
	dijkstra(grafo, 'c', 'd')
	pass # Replace with function body.

func dijkstra(grafo,inicio,meta):
	var distanciaCorta = {}
	var previo = {}
	var porRevisar = grafo
	var infinito = 99999999999999999
	var camino = []
	for nodo in porRevisar:
		distanciaCorta[nodo] = infinito
		distanciaCorta[inicio] = 0
	while porRevisar:
		var mejorNodo = null
		for nodo in porRevisar:
			if mejorNodo == null:
				mejorNodo = nodo
			elif distanciaCorta[nodo] < distanciaCorta[mejorNodo]:
				mejorNodo = nodo
		for hijo in grafo[mejorNodo].keys():
			for peso in grafo[mejorNodo].values():
				if peso + distanciaCorta[mejorNodo] < distanciaCorta[hijo]:
					distanciaCorta[hijo] = peso + distanciaCorta[mejorNodo]
					previo[hijo] = mejorNodo
		porRevisar.erase(mejorNodo)
	var actual = meta
	while actual != inicio:
		camino.insert(0,actual)
		actual = previo.get(actual)
	camino.insert(0,inicio)
	if distanciaCorta[meta] != infinito:
		print('La distancia es ' + str(distanciaCorta[meta]))
		print('El camino es ' + str(camino))