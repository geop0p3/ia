grafo = {
's':{'b':7,'a':2},
'b':{'a':2,'c':1},
'a':{'b':3,'c':8,'d':5},
'c':{'d':4},
'd':{'c':5}
}

def dijkstra(grafo,inicio,meta):
    distanciaCorta = {}
    previo = {}
    porRevisar = grafo
    infinito = float("inf")
    camino = []
    for nodo in porRevisar:
        distanciaCorta[nodo] = infinito
    distanciaCorta[inicio] = 0

    while porRevisar:
        mejorNodo = None
        for nodo in porRevisar:
            if mejorNodo is None:
                mejorNodo = nodo
            elif distanciaCorta[nodo] < distanciaCorta[mejorNodo]:
                mejorNodo = nodo

        for hijo, peso in grafo[mejorNodo].items():
            if peso + distanciaCorta[mejorNodo] < distanciaCorta[hijo]:
                distanciaCorta[hijo] = peso + distanciaCorta[mejorNodo]
                previo[hijo] = mejorNodo
        porRevisar.pop(mejorNodo)

    actual = meta
    while actual != inicio:
        try:
            camino.insert(0,actual)
            actual = previo[actual]
        except KeyError:
            print('No se puede :o')
            break
    camino.insert(0,inicio)
    if distanciaCorta[meta] != infinito:
        print('La distancia es ' + str(distanciaCorta[meta]))
        print('El camino es ' + str(camino))


dijkstra(grafo, 'c', 'd')
