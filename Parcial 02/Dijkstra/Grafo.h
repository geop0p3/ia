#pragma once
#include <iostream>
#include "ListaTemplate.h"

template <typename Type>
class Grafo
{
public:
	struct Orilla
	{
		Orilla() {}
		Orilla(int desde, int a, int c) { desdeVertice = desde; alVertice = a; costo = c; }
		int alVertice;
		int desdeVertice;
		int costo;
	};

	struct Vertice
	{
		Type elemento;
		ListaTemplate<Orilla> orillas;
		Vertice& operator=(const Vertice& temp) {
			if (this != &temp) {
				this->elemento = temp.elemento;
				this->orillas = temp.orillas;
				this->numorillas = temp.numorillas;
				this->distancia = temp.distancia;
				this->visitado = temp.visitado;
				this->predecesor = temp.predecesor;
			}
			return *this;
		}

		friend bool operator>(const Vertice& left, const Vertice& right) { return left.distancia > right.distancia; }
		friend std::ostream& operator<<(std::ostream& out, const Vertice& v)
		{
			out << "Vertice: " << v.elemento << "\n";
			return out;
		}

		void AgregarOrilla(int desde, int haciaVertice, int costo)
		{
			++numOrillas;
			this->orillas.PushBack(Orilla(desde, haciaVertice, costo));
		}
		Vertice() {}
		Vertice(Type a) { elemento = a; numOrillas = 0; }

		int numOrillas = 0;

		int distancia = 0;
		Vertice* predecesor = nullptr;
		bool visitado = false;
	};

private:
	ListaTemplate<Vertice> grafo;
public:
	Grafo() {}
	virtual ~Grafo() { Clear(); }

	int AgregarVertice(const Type& value)
	{
		grafo.PushBack(value);
		return grafo.count() - 1;
	}

	 Vertice& operator[](int index)
	 {
		 return grafo[index];
	 }

	 void Clear()
	 {
		 grafo.Clear();
	 }
};

