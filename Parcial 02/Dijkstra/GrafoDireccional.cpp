#include <iostream>
#include "ColaPrioridad.h"
#include <string>

using namespace std;

#include "Grafo.h"

template <typename type>
bool MayorQue(const type& _left, const type& _right)
{
	return _left > _right;
}


void Dijkstra(Grafo<string>& grafo, int size, int from, int to)
{
	cout << "Beginning of Dijkstra\n";
	ColaPrioridad<Grafo<string>::Vertice> pq = ColaPrioridad<Grafo<string>::Vertice>(&MayorQue);

	for (int i = 0; i < size; i++) {
		grafo[i].distancia = i * 10000;
		pq.Enqueue(grafo[i]);
	}
	cout << "Priority Queue at start:\n";
	pq.PrintQueue();

	Grafo<string>::Vertice* v;
	Grafo<string>::Vertice temp;
	Grafo<string>::Vertice* cCon;
	while (!pq.Empty() && !grafo[to].visitado)
	{
		v = pq.Front();
		v->visitado = true;

		cout << "Empezando a revisar: " << v->elemento << "\n";
		for (int conexion = 0; conexion < v->numOrillas; conexion++) 
		{
			cCon = &grafo[v->orillas[conexion].alVertice];
			if (!cCon->visitado) 
			{
				if (cCon->predecesor == nullptr) 
				{
					cCon->predecesor = &grafo[v->orillas[conexion].desdeVertice];
					cCon->distancia = grafo[v->orillas[conexion].desdeVertice].distancia + 
						grafo[v->orillas[conexion].desdeVertice].orillas[conexion].costo;
				}
				else
				{
					if (cCon->distancia > (v->distancia + v->orillas[conexion].costo))
					{
						cCon->predecesor = &grafo[v->orillas[conexion].desdeVertice];
						cCon->distancia = grafo[v->orillas[conexion].desdeVertice].distancia + 
							grafo[v->orillas[conexion].desdeVertice].orillas[conexion].costo;
						cout << "Nueva distancia de " << cCon->elemento << " es de " << cCon->distancia;
					}
				}
			}
		}
		cout << "\n";
		cout << "Termine de revisar " << v->elemento << "\n";
		pq.DeQueue();
		if (!pq.Empty())
		{
			cout << "------------------\n";
			cout << "PQ ACTUALIZADA\n";
			cout << "------------------\n";
			pq.PrintQueue();
		}
	}//fin while

	Grafo<string>::Vertice* current = &grafo[to];
	cout << "End of Dijkstra\n";
	cout << "Camino elegido con costo" << grafo[to].distancia << "\n";
	while (current != nullptr)
	{
		cout << "" << current->elemento << "-";
		current = current->predecesor;
	}
}

int main()
{
	Grafo<string> grafo = Grafo<string>();
	grafo.AgregarVertice("S");
	grafo.AgregarVertice("A");
	grafo.AgregarVertice("B");
	grafo.AgregarVertice("C");
	grafo.AgregarVertice("D");

	//Conexiones S
	grafo[0].AgregarOrilla(0, 1, 2);
	grafo[0].AgregarOrilla(0, 2, 7);

	//Conexiones A
	grafo[1].AgregarOrilla(1, 2, 3);
	grafo[1].AgregarOrilla(1, 3, 8);
	grafo[1].AgregarOrilla(1, 4, 5);

	//Conexiones B
	grafo[2].AgregarOrilla(2, 3, 1);
	grafo[2].AgregarOrilla(2, 1, 3);

	//Conexiones C
	grafo[3].AgregarOrilla(3, 4, 4);

	//Conexiones D
	grafo[4].AgregarOrilla(4, 3, 5);

	Dijkstra(grafo, 5, 0, 4);

	system("pause");

	return 0;
}