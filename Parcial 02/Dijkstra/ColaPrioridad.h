#pragma once
#ifndef COLAPRIORIDAD_H_
#define COLAPRIORIDAD_H_
#include <iostream>

using std::cout;
template <typename type>
class ColaPrioridad
{
	struct Node 
	{
		type elemento;
		Node* next;
		Node* prev;
		Node(type _elemento) : elemento(_elemento), next(0), prev(0) 
		{

		}
	};

	Node* Head;
	Node* Tail;
	bool(*fncompare)(const type&, const type&);

public:
	ColaPrioridad(bool (*_funcion)(const type& x, const type& y)): fncompare(_funcion)
	{
		Head = Tail = 0;
	}
	~ColaPrioridad() 
	{
		Clear();
	}

	//Insertar
	void Enqueue(const type& val) 
	{
		Node* right = new Node(val);
		//Si es el primer elemento, insertado nada mas
		if (!Tail) 
		{
			Head = Tail = right;
		}
		else 
		{
			Node* curr = Head;
			Node* temp = Head;

			if (Head->next == nullptr) 
			{
				if (fncompare(Head->elemento, right->elemento)) 
				{
					Head = right;
					right->next = curr;
					curr->prev = right;
				}
				else 
				{
					Tail = right;
					right->prev = curr;
					curr->next = right;
				}
			}
			else 
			{
				while (Head->next) 
				{
					cout << "Compare" << curr->elemento << "vs" << right->elemento << "\n";
					if (fncompare(curr->elemento, right->elemento)) 
					{
						right->next = curr;
						right->prev = curr->prev;
						if (curr == Head) 
						{
							Head = right;
						}
						else 
						{
							curr->prev->next = right;
						}
						curr->prev = right;
						break;
					}

					if (curr == Tail) 
					{
						Tail = right;
						curr->next = right;
						right->prev = curr;
						break;
					}
					else
					{
						curr = curr->next;
					}
				}
			}
		}
	}
	void PrintQueue()
	{
		Node* temp = Head;
			do
			{
				cout << "Valor " << temp->elemento << "\n";
				if (temp == Tail) 
				{
					break;
				}
				else 
				{
					temp = temp->next;
				}
			} while (true);
	}

	void DeQueue() 
	{
		if (Head) 
		{
			Node* temp = Head;
			Head = Head->next;
			delete temp;
		}
	}

	type* Front() const
	{
		return &Head->elemento;
	}
	void Clear() 
	{
		while (Head) 
		{
			DeQueue();
		}
		Head = Tail = 0;
	}

	bool Empty() const 
	{
		return Head == 0;
	}
};
#endif

