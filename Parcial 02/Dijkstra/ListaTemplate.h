#pragma once
#ifndef LISTATEMPLATE_H_
#define LISTATEMPLATE_H_
#include <iostream>

using std::cout;

template <typename type>
class IteratorTemplate;
template <typename type>

class ListaTemplate {
	struct NodeTemplate {
		type elemento;
		NodeTemplate * next = nullptr;

		NodeTemplate(type _elemento) : elemento(_elemento), next(0) 
		{

		}
	};

	NodeTemplate* Head = nullptr;

public:
	ListaTemplate() {
		Head = 0;
	}
	~ListaTemplate() {

	}

	int count() 
	{
		IteratorTemplate<type> it(*this);
		it.Begin();
		int cont(0);
		while (it.End() == false)
		{
			++cont;
			++it;
		}

		return cont;
	}

	void PushFront(const type& val) {
		NodeTemplate *temp = new NodeTemplate(val);

		if (Head)
		{
			temp->next = Head;
		}
		else {
			temp->next = nullptr;
		}

		Head = temp;
	}

	void PushBack(const type& val) {
		if (!Head) {
			PushFront(val);
		}
		else {
			NodeTemplate* temp = new NodeTemplate(val);
			IteratorTemplate <type> it(*this);
			while (it.End() == false) {
				++it;
			}

			temp->next = 0;
			it.prev->next = temp;
		}
	}

	void Clear() {
		NodeTemplate* temp;
		while (Head != NULL) {
			temp = Head;
			Head = Head->next;
			delete temp, temp = nullptr;
		}

		Head = nullptr;
	}

	type& operator[](int index) {
		IteratorTemplate<type> it(*this);
		for (int i = 0; i < index; i++) {
			if (it.End()) break;
			++it;
		}
		return it.current->elemento;
	}

	friend class IteratorTemplate<type>;
};

template <typename type>
class IteratorTemplate 
{
	typename ListaTemplate<type>::NodeTemplate* current = 0;
	typename ListaTemplate<type>::NodeTemplate* prev = 0;
	typename ListaTemplate<type>* MiLista;

public:
	IteratorTemplate(ListaTemplate <type> & lista) 
	{
		MiLista = &lista;
		current = 0;
		Begin();
	}

	void Begin() {
		current = MiLista->Head;
		prev = 0;
	}

	bool End() const
	{
		return (current == 0);
	}

	IteratorTemplate<type>& operator++() {
		if (current) {
			prev = current;
			current = current->next;
		}
		return *this;
	}

	friend class ListaTemplate<type>;
};

#endif